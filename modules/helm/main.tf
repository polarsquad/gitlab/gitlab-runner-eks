terraform {
  backend "s3" {}
  required_version = "~> 0.13"
}


provider "aws" {
  region  = var.aws_region
  version = "= 3.11.0"
  profile = var.profile
}
data "aws_eks_cluster" "cluster" {
  name = var.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = var.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.13"
}

provider "helm" {
  version = "= 1.3.2"

  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    load_config_file       = false
  }
}

resource "helm_release" "cluster-autoscaler" {
  name      = "autoscaler"
  repository = "https://kubernetes.github.io/autoscaler"
  chart     = "cluster-autoscaler-chart"
  namespace = "kube-system"
  version   = "1.1.0"
  set {
    name = "cloudProvider"
    value = "aws"
  }
  set {
    name = "autoDiscovery.clusterName"
    value = var.cluster_id
  }
  set {
    name = "rbac.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = var.role-arn
  }

  set {
    name = "rbac.serviceAccount.name"
    value = var.k8s_autosaler_service_account_name
  }
  set {
    name = "awsRegion"
    value = var.aws_region
  }

  set {
    name = "extraArgs.ignore-daemonsets-utilization"
    value = "true"
  }
  set {
    name = "extraArgs.skip-nodes-with-local-storage"
    value = "false"
  }
  set {
    name = "extraArgs.scale-down-unneeded-time"
    value = "30m0s"
  }

}

resource "kubernetes_namespace" "gitlab-runner" {
  metadata {
    annotations = {
      name = "gitlab-runner"
    }
    name = "gitlab-runner"
  }
}

resource "helm_release" "gitlab-runner" {
  depends_on = [kubernetes_namespace.gitlab-runner]
  name      = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart     = "gitlab-runner"
  namespace = "gitlab-runner"
  version   = "0.21.1"
  set {
    name = "runners.cache.s3BucketLocation"
    value = var.aws_region
  }
  values = [
    file("${path.module}/values/gitlab-runner.yaml")
  ]

}

resource "kubernetes_namespace" "argocd" {
  metadata {
    annotations = {
      name = "argocd"
    }
    name = "argocd"
  }
}

resource "helm_release" "argocd" {
  depends_on = [kubernetes_namespace.argocd]
  name          = "argocd"
  namespace     = "argocd"
  repository    = "https://argoproj.github.io/argo-helm"
  chart         = "argo-cd"
  values = [
    file("${path.module}/values/argocd.yaml")
  ]
}

